# Termion Carriage Return
Writes `triggered` to the screen if a single carriage return is detected.
Exit with `e`.

# Usage
```bash
cargo run
```
