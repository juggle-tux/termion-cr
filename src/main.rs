use std::io::{stdin, stdout, Read, Write};
use std::sync::mpsc::{self, TryRecvError};
use std::thread;
use termion::raw::IntoRawMode;
use termion::screen::*;

fn main() {
    let mut stdin = stdin();
    let mut screen = AlternateScreen::from(stdout().into_raw_mode().unwrap());

    let (tx, rx) = mpsc::channel::<([u8; 512], usize)>();

    let _terminal_stdin = thread::spawn(move || loop {
        let mut data = [0; 512];
        let n = stdin.read(&mut data[..]).unwrap();
        tx.send((data, n)).unwrap();
    });

    write!(screen, "{}{}Writes `triggered` to the screen if a single carriage return is detected.\r\nExit with 'e'.\r\n", termion::clear::All, termion::cursor::Goto(1,1)).unwrap();
    screen.flush().unwrap();

    loop {
        let data: [u8; 512];
        let n: usize;
        match rx.try_recv() {
            Ok((rx_data, rx_n)) => {
                data = rx_data;
                n = rx_n;
            }
            Err(TryRecvError::Disconnected) => {
                eprint!("{}Error: Stdin reading thread stopped.\n\r", ToMainScreen,);
                break;
            }
            Err(TryRecvError::Empty) => {
                continue;
            }
        }

        if n == 1 {
            if data[0] == b'\r' {
                write!(screen, "triggered\r\n").unwrap();
                screen.flush().unwrap();
            } else if data[0] == b'e' {
                break;
            }
        }
        screen.write_all(&data).unwrap();
        screen.flush().unwrap();
    }
}
